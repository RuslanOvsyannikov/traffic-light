from urllib.request import Request, urlopen
from urllib.error import HTTPError
import xml.etree.ElementTree as ET
import sys


URL = 'https://tlight.biz/media/zadane/tz1.php?id=%s' % sys.argv[1]
METHOD = 'GET'
HEADERS = {
    'Content-Type': 'application/json',
    'X-Auth': 'tokentoken'
}
DATA = None

TREASURE_XML_PATH = ('status',)


def send_request_traffic_light(url, method, headers, data):
    req = Request(url, data=data, headers=headers, method=method)
    try:
        response = urlopen(req)
    except HTTPError as e:
        print(e.read())
        return
    if response.getcode() != 200:
        print(response.read())
        return
    elem = ET.fromstring(response.read())
    for tag in TREASURE_XML_PATH:
        elem = elem.find(tag)
    print(elem.text)

send_request_traffic_light(URL, METHOD, HEADERS, DATA)

