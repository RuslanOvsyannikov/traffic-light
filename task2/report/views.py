from django.shortcuts import render, HttpResponse
from django.db import connection
# Create your views here.

def show_report(request):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT group_statistic.days,
          IFNULL(group_statistic.clicks_uniq, 0),
          IFNULL(ords.handle, 0),
          IFNULL(ords.accept,0),
          IFNULL(ords.cancel,0),
          IFNULL(ords.asaffiliate, 0)
        FROM (SELECT DATE(statistic.date) AS days, SUM(statistic.clicks_uniq) AS clicks_uniq
          FROM statistic
          GROUP BY days) AS group_statistic
        LEFT join (SELECT DATE(orders.created_at) AS days,
          SUM(IF(orders.status = 0, 1, 0)) AS handle,
          SUM(IF(orders.status >= 10, 1, 0)) AS accept,
          SUM(IF(orders.status < 0, 1, 0)) AS cancel,
          SUM(orders.affiliate_fee) AS asaffiliate
        FROM orders
        GROUP BY days) AS ords ON ords.days = group_statistic.days
        ORDER BY group_statistic.days DESC;
        """)
    context = {'rows': cursor.fetchall()}
    print(context['rows'][0])
    return render(request, 'report/show_all.html', context)

